# Resolve environment variables in Windows file paths

Resolve path prefixes such as %GOPATH% and %USERPROFILE% at the start of file paths on Windows.

Example: 
```
fmt.Println(windowsPathEnv.Resolve("%GOPATH%/pkg/stu-b-doo"))
fmt.Println(windowsPathEnv.Resolve("%USERPROFILE%/Documents"))
```
