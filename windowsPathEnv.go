// windowsPathEnv resolves environment variables at the start of filepaths
package windowsPathEnv

import (
	"strings"
	fp "path/filepath"
	"os"
)

// Resolve first element of a filepath as environment variable if enclosed in %. Only the first path element is considered as an environment variable. Eg: 
// %GOPATH%/bin/gitlab.com/stu-b-doo/
func Resolve(filepath string) (out string) {

	// return the original filepath unchanged unless we get to the end
	out = filepath

	// return unless strings starts with %
	if !strings.HasPrefix(filepath, "%") {
		return
	}

	// return unless there's a second %
	trim := strings.TrimPrefix(filepath, "%")
	i := strings.Index(trim, "%")
	if i == -1 {
		return
	}

	// check if substr between two % is the name of an existing env var
	val, ok := os.LookupEnv(trim[:i])
	if !ok {
		return
	}

	// env var value will use os path separator
	remainder := fp.FromSlash(trim[i+1:])

	// check the remainder starts with path separateor
	if !strings.HasPrefix(remainder, "\\") {
		return
	}

	// prepend the value to the remainder of the path
	return val + remainder
}

